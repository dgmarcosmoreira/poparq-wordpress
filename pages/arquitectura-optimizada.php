<?php /* Template Name: Arquitectura optimizada */ ?>

<?php get_header(); ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php
        if (!is_mobile()){
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
        }else {
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
        }
      ?>
      <section id="main" class="mb-4 full-height relative">
      <div class="gradient"></div>
      <div class="image-cover" style="background-image:url('<?php echo $featured_img_url; ?>')"></div>
        <div class="container">
          <div class="row align-items-center full-height">
              <div class="col-lg-6 texto-backdrop">
                <h1 class="text-white"><?php the_title(); ?></h1>
                <div class="text-white lead">
                    <?php the_content(); ?>
                </div>
                  <a class="btn btn-warning" href="#description">CONOCE MÁS</a>
              </div>
          </div>
        </div>
  </section>

<?php get_template_part('inc/section','icons'); ?>

<section id="materiales" class="bg-light">
    <div class="spacer-2"></div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="line-block">
          <h2 class="lined"><?php the_field('ao_titulo'); ?></h2>
          <p class="lead">
              <?php the_field('ao_lead'); ?>
          </p>
        </div>
      </div>
    </div>
    <div class="spacer-2"></div>
    <div class="row">
        <?php $images = get_field('ao_logos');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)

        if( $images ): ?>

            <?php foreach( $images as $image ): ?>
                <div class="col-lg-3 col-md-6 col-6 mb-5 mb-lg-0">
                    <?php echo wp_get_attachment_image( $image['ID'], $size, '', array( "class" => "img-fluid w-100" ) ); ?>
                </div>

            <?php endforeach; ?>

        <?php endif; ?>

      </div>
    <div class="spacer-2"></div>
  </div>
</section>
  <?php endwhile; endif; ?>

  <?php get_footer();?>
