<?php /* Template Name: Estudio */ ?>

<?php get_header(); ?>

  <section id="main" class="mb-4 full-height relative">
      <div class="gradient"></div>
      <div class="image-cover" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/estudio/estudio.jpg')"></div>
        <div class="container">
          <div class="row align-items-center full-height">
              <div class="col-lg-6 texto-backdrop">
                <h1 class="text-white">Conocé Poparq</h1>
                <p class="text-white">Somos un grupo interdisciplinario de profesionales que busca hacer más accesibles procesos de construcción manteniendo la calidad del sistema de construcción tradicional. Queremos acompañarte y ser parte del proceso de creación de tu nuevo hogar.</p>
                <a href="#estudio" class="btn btn-warning" title="Nosotros">CONOCENOS</a>
              </div>
          </div>
        </div>
  </section>
  <div class="spacer-2"></div>

  <section id="estudio">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="line-block">
            <h2 class="lined">Nuestra filosofía</h2>
            <p class="lead">
              Elegí el modelo que mejor se adapta a tu familia y personalizarlo. ¡Podés elegir las terminaciones y agregar muchas cosas para que quede como querés!
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="spacer-2"></div>

<div class="container mt-3" id="nosotros">
  <div class="card-deck">
    <?php $staff=get_field('staff', 'option'); ?>
    <?php foreach ($staff as $item){ ?>
        <?php if ($item['destacado']){ ?>
        <div class="card text-white mb-3" style="background: <?php echo $item['color'] ?>">
          <img class="card-img-top" src="<?php echo $item['foto'] ?>" alt="<?php echo $item['nombre'] ?>">
          <div class="card-body">
            <h5 class="card-title mb-0"><?php echo $item['nombre'] ?></h5>
            <p><?php echo $item['cargo'] ?></p>
            <p class="card-text"><?php echo $item['resumen'] ?></p>
          </div>
        </div>
    <?php }} ?>
  </div>
</div>

<div class="container mt-3">
  <div class="card-deck">
    <?php foreach ($staff as $item){ ?>
        <?php if (!$item['destacado']){ ?>
          <div class="card mb-3">
              <img class="card-img-top" src="<?php echo $item['foto'] ?>" alt="<?php echo $item['nombre'] ?>">
              <div class="p-4">
                  <h5 class="card-title mb-0"><?php echo $item['nombre'] ?></h5>
                  <p><?php echo $item['cargo'] ?></p>
              </div>
          </div>
      <?php }} ?>
  </div>
</div>

<div class="spacer-2"></div>

<?php get_template_part('inc/section','formulario'); ?>

<?php get_footer(); ?>

