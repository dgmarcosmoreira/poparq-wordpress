<section id="historias" class="relative">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-6">
        <div class="line-block">
          <h2 class="lined">Historias</h2>
          <p class="lead">
            Conocé la experiencia de nuestros clientes.
          </p>
        </div>
      </div>
    </div>
    <div class="spacer-1"></div>
    <div class="d-block">
      <div class="owl-theme slider-historias">
          <?php
          $args = array(
              'post_type'      => 'historias',
              'posts_per_page' => -1,
              'post_parent' => 0,
              'order'          => 'ASC',
              'orderby'        => 'menu_order'
          );
          ?>
          <?php query_posts($args); ?>
          <?php while ( have_posts() ) : the_post(); ?>
              <?php get_template_part('inc/card', 'historias'); ?>
          <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>
