<div class="col-lg-4 item">
  <div class="card h-100">
    <div class="card-body">
      <p class="lead">
      <span class="display-2 text-primary">01</span><br />
        <strong>Elegí</strong> el modelo que mejor se adapte a las necesidades de tu familia.</p>
    </div>
  </div>
</div>
<div class="col-lg-4 item">
  <div class="card h-100 ">
    <div class="card-body">
      <p class="lead">
      <span class="display-2 text-primary">02</span><br />
        <strong>Personalizá</strong> tu hogar y visualizá la actualización del precio al instante.</p>
    </div>
  </div>
</div>
<div class="col-lg-4 item">
  <div class="card h-100">
    <div class="card-body">
      <p class="lead">
      <span class="display-2 text-primary">03</span><br />
        <strong>¡Consultanos!</strong> Nos llegará un mensaje con las opciones que seleccionaste.</p>
    </div>
  </div>
</div>
