<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center text-md-left">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg"
                     alt="Poparq - Arquitectura Optimizada" class="mb-4">
                <p class="text-white">
                    (011) 5505.8658 <br>
                    hola@poparq.com.ar <br>
                    Calle 0 y 701, Chapadmalal <br>
                </p>
            </div>
            <div class="col-md-3 text-white d-none d-md-block">
                <ul class="nav flex-column">
                    <h5>POPARQ</h5>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/estudio">Estudio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/historias-de-clientes">Historias</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/arquitectura-optimizada">Arquitectura
                            Optimizada</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url() ?>/blog">Blog</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-3 text-white d-none d-md-block">
                <h5>CASAS DE 1 O 2 AMBIENTES</h5>

                <ul class="nav flex-column">
                    <?php
                    $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'modelo',
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key' => 'ambientes',
                                'value' => array(1, 2),
                                'compare' => 'IN',
                            ),
                        )
                    ));
                    foreach ($posts as $post) { ?>
                        <?php if (!is_parent()) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo get_the_permalink($posts->ID) ?>">
                                    <?php
                                    $parent = wp_get_post_parent_id(get_the_id());
                                    echo get_the_title($parent);
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>

            </div>
            <div class="col-md-3 text-white d-none d-md-block">
                <h5>CASAS DE 3 O 4 AMBIENTES</h5>
                <ul class="nav flex-column">

                    <?php
                    $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'modelo',
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key' => 'ambientes',
                                'value' => array(3, 4),
                                'compare' => 'IN',
                            ),
                        )
                    ));
                    foreach ($posts as $post) { ?>
                        <?php if (!is_parent()) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo get_the_permalink($posts->ID) ?>">
                                    <?php

                                    $parent = wp_get_post_parent_id(get_the_id());
                                    echo get_the_title($parent);
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/vendor/owl.carousel.min.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
