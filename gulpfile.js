"use strict";

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    image = require('gulp-image'),
    htmlreplace = require('gulp-html-replace'),
    cssmin = require('gulp-cssmin');


gulp.task('image', function () {
    gulp.src('../uploads/*/*/*')
        .pipe(image())
        .pipe(gulp.dest('dist/assets/img'));
});

gulp.task('default', ['image']);


gulp.task("concatScripts", function () {
    return gulp.src([
        'assets/js/vendor/jquery-3.3.1.slim.min.js',
        'assets/js/vendor/popper.min.js',
        'assets/js/vendor/bootstrap.min.js',
        'assets/js/vendor/jquery-simple-validator.min.js',
        'assets/js/vendor/owl.carousel.min.js',
        'assets/js/functions.js',
        'assets/js/forms.js'
    ])
        .pipe(maps.init())
        .pipe(concat('main.js'))
        .pipe(maps.write('./'))
        .pipe(gulp.dest('assets/js'))
        .pipe(browserSync.stream());
});

gulp.task("minifyScripts", ["concatScripts"], function () {
    return gulp.src("assets/js/main.js")
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('compileSass', function () {
    return gulp.src("assets/css/main.scss")
        .pipe(maps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(maps.write('./'))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
});

gulp.task("minifyCss", ["compileSass"], function () {
    return gulp.src("assets/css/main.css")
        .pipe(cssmin())
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('dist/assets/css'));
});

gulp.task('watchFiles', function () {
    gulp.watch('assets/css/**/*.scss', ['compileSass']);
    gulp.watch('assets/js/*.js', ['concatScripts']);
})

gulp.task('clean', function () {
    del(['dist', 'assets/css/main.css*', 'assets/js/main*.js*']);
});

gulp.task('renameSources', function () {
    return gulp.src(['*.html', '*.php'])
        .pipe(htmlreplace({
            'js': 'assets/js/main.js',
            'css': 'assets/css/main.css'
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('copy', function () {
    gulp.src('pages/*.php')
        .pipe(gulp.dest('dist/pages'));
    gulp.src('inc/*.php')
        .pipe(gulp.dest('dist/inc'));
});
gulp.task("build", ['minifyScripts', 'minifyCss', 'copy', 'image'], function () {
    return gulp.src([
        '*.html',
        '*.php',
        'favicon.ico',
        "assets/img/**"
    ], {base: './'})
        .pipe(gulp.dest('dist'));
});

gulp.task('serve', ['watchFiles'], function () {
    browserSync.init({
        proxy: "http://localhost/poparq",
        open: 'extenal'
    });

    gulp.watch("assets/css/**/*.scss", ['watchFiles']);
    gulp.watch(['*.html', '*.php']).on('change', browserSync.reload);
});

gulp.task("default", ["clean", 'build'], function () {
    gulp.start('renameSources');
});
