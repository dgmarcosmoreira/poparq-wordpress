<?php get_header(); ?>
<section id="main" class="relative main single">
    <?php echo $nombre; echo $localidad; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 text-center">
                <h2 class="display-1 text-primary">404</h2>
                    <h1>No encontramos la página</h1>
                    <div class="line-block">
                        <h5 class="lined">Intentá nuevamente o comunicate con nosotros a hola@poparq.com.ar </h5>
                    </div>

            </div>
            <div class="my-4 col-12"></div>
            <div class="col-12 text-center">
                <a href="<?php echo home_url() ?>/contacto" class="btn btn-primary btn-lg "> CONTACTO</a>
            </div>
        </div>
    </div>
    <div class="spacer-1"></div>
</section>
<?php get_footer(); ?>
<!-- IE needs 512+ bytes: http://blogs.msdn.com/b/ieinternals/archive/2010/08/19/http-error-pages-in-internet-explorer.aspx -->
