  <?php get_header(); ?>

    <section id="main" class="mb-4 home main">
      <div class="relative">
        <div class="backdrop">
          <div class="container h-100">
            <div class="row justify-content-center align-items-center">
              <div class="col-9 col-md-6 col-lg-6 text-center">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/Home.png" class="img-fluid mx-auto d-none d-lg-block" style="margin-bottom:-8rem;" />
                  <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/Home.png" class="img-fluid mx-auto d-block d-lg-none mt-4" />
              </div>
              <div class="col-lg-6 text-center text-lg-left">
                  <div class="mt-4 d-block d-lg-none"></div>
                  <h1>Tu casa <span class="text-success">simple</span><br />y <span class="text-success">sin vueltas</span></h1>
                <p class="lead">Poparq es una nueva manera de construir. Con precios cerrados, tiempos preestablecidos y la mejor calidad de materiales. Simple y sin sorpresas.</p>
                <p><a class="btn btn-warning" href="<?php echo home_url() ?>/catalogo" role="button">ELEGÍ TU CASA</a></p>
              </div>
            </div>
          </div>
            <div class="spacer-2 d-block d-md-none"></div>
        </div>
      </div>
    </section>

    <div class="spacer-2 d-none d-lg-block"></div>

    <?php get_template_part('inc/section','icons'); ?>
    <?php get_template_part('inc/section', 'personalizar'); ?>

    <section class="section" id="catalogo">
      <div class="container">
        <div class="row">
          <div class="col-md-6 text-center text-md-left">
            <h4>Modelos disponibles</h4>
          </div>
        </div>
        <div class="row">
            <?php
                $args = array(
                'post_type'      => 'modelo',
                'posts_per_page' => -1,
                    'post_parent' => 0,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
                );
             ?>
            <?php query_posts($args); ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php include('inc/card-product.php'); ?>
            <?php endwhile; ?>
        </div>
        <div class="spacer-2"></div>
        <div class="row justify-content-center">
          <div class="col-lg-6 text-center">
            <p class="lead">
              ¿Ninguno se adapta a lo que necesitás? ¡Consultanos por modelos personalizados!
            </p>
            <p>
              <a href="<?php echo home_url(); ?>/contacto" class="btn btn-outline-primary">CONSULTANOS</a>
            </p>
          </div>
        </div>
      </div>
    </section>

    <section id="chapadmalal" class="bg-success">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-4 text-center text-lg-left">
                <div class="spacer-2"></div>
                <?php $pagina = get_post( 260 ); ?>
                <h2 class="text-white"><?php echo $pagina->post_title; ?></h2>
                <p class="text-white">
                    <?php echo $pagina->post_excerpt; ?>
                </p>
                <p>
                  <a href="http://chapadmalal.org.ar" class="btn btn-outline-light">CONOCÉ MÁS</a>
                </p>
            </div>
            <div class="col-md-8 text-center text-md-right">
              <div class="image pt-5">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/chapadmalal.jpg" class="img-fluid rounded-circle"/>
              </div>
            </div>
          </div>
        </div>
    </section>

    <div class="spacer-2"></div>
    <div class="spacer-2"></div>

    <?php get_template_part('inc/section','historias'); ?>

    <div class="spacer-2"></div>

  <?php get_template_part('inc/section','formulario'); ?>


<?php get_footer(); ?>
