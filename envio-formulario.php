<?php 
/**
 * Template Name: Envio Formulario
 *
 */
//header('Content-type: application/json');
//header("Access-Control-Allow-Origin: *");
//ob_end_flush();
?>
<?php get_header(); ?>

<?php

$nombre = $_POST['nombre'];
$email = $_POST['email'];
$tlf = $_POST['telefono'];
$localidad = $_POST['localidad'];
$comentarios = $_POST['comentarios'];

    /// ADMINISTRADOR ///////
///
///
///
//$emailTo = get_option('admin_email');
$emailTo = 'chelletjd@gmail.com';


$subject = 'From ' . $nombre;
$body = '';
$body .= 'Bienvenido, '.$nombre.'. La información recibida fue la siguiente: Telefono: '.$tlf.', Email: '.$email.', Localidad: '.$localidad.' y comentarios: '.$comentarios."\n\n\n";
$headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
wp_mail($emailTo, $subject, $body, $headers);
$emailSent = true;


 ///// USUARIO /////

//$emailTo = $email;
$emailTo = 'chellet.jd@gmail.com';
$subject = 'From ' .$nombre;
$body = '';
$body .= 'Bienvenido, '.$nombre.'. La información recibida fue la siguiente: Telefono: '.$tlf.', Email: '.$email.', Localidad: '.$localidad.' y comentarios: '.$comentarios."\n\n\n";
$headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
wp_mail($emailTo, $subject, $body, $headers);
$emailSent = true;

$post_arr = array(
    'post_title' => $name.' - '. $email ,
    'post_type' => 'pedido',
    'post_content' => $body,
    'post_status' => 'publish',
    'post_author' => get_current_user_id());

wp_insert_post($post_arr);

if (isset($emailSent) && $emailSent == true) {
    $return = 'ok';

} else {
    $return = 'no';
}
json_encode($return);

?>
<section id="main" class="relative main single">
    
    <div class="container <?php if($return != 'ok') { echo "d-none";} ?>">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <h1>¡Genial!</h1>
                <div class="line-block">
                    <h2 class="lined">Tu correo ha sido enviado exitosamente.</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container <?php if($return != 'no') { echo "d-none";} ?>">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <h1>¡Lo sentimos!</h1>
                <div class="line-block">
                    <h2 class="lined">Tuvimos un problema al enviar tu información.</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer-1"></div>
</section>
<?php get_footer(); ?>