var wskCheckbox = function() {
  var wskCheckboxes = [];
  var SPACE_KEY = 32;

  function addEventHandler(elem, eventType, handler) {
    if (elem.addEventListener) {
      elem.addEventListener (eventType, handler, false);
    }
    else if (elem.attachEvent) {
      elem.attachEvent ('on' + eventType, handler);
    }
  }

  function clickHandler(e) {
    e.stopPropagation();
    if (this.className.indexOf('checked') < 0) {
      this.className += ' checked';
    } else {
      this.className = 'chk-span';
    }
  }
  function keyHandler(e) {
    e.stopPropagation();
    if (e.keyCode === SPACE_KEY) {
      clickHandler.call(this, e);
      // Also update the checkbox state.

      var cbox = document.getElementById(this.parentNode.getAttribute('for'));
      cbox.checked = !cbox.checked;
    }
  }

  function clickHandlerLabel(e) {
    var id = this.getAttribute('for');
    var i = wskCheckboxes.length;
    while (i--) {
      if (wskCheckboxes[i].id === id) {
        if (wskCheckboxes[i].checkbox.className.indexOf('checked') < 0) {
          wskCheckboxes[i].checkbox.className += ' checked';
        } else {
          wskCheckboxes[i].checkbox.className = 'chk-span';
        }
        break;
      }
    }
  }

  function findCheckBoxes() {
    var labels =  document.getElementsByTagName('label');
    var i = labels.length;
    while (i--) {
      var posCheckbox = document.getElementById(labels[i].getAttribute('for'));
      if (posCheckbox !== null && posCheckbox.type === 'checkbox') {
        var text = labels[i].innerText;
        var span = document.createElement('span');
        span.className = 'chk-span';
        span.tabIndex = i;
        labels[i].insertBefore(span, labels[i].firstChild);
        addEventHandler(span, 'click', clickHandler);
        addEventHandler(span, 'keyup', keyHandler);
        addEventHandler(labels[i], 'click', clickHandlerLabel);
        wskCheckboxes.push({'checkbox': span,
            'id': labels[i].getAttribute('for')});
      }
    }
  }

  return {
    init: findCheckBoxes
  };
}();

wskCheckbox.init();

var $home_url= 'http://c0300309.ferozo.com/zanoli/wp-content/themes/zanoli/inc/';

function submitForm_contact() {

    $('#contact-form #submit').text('ENVIANDO...');

    var paquete = $('#contact-form #paquetes').val();
     var name = $('#contact-form #name').val();
     var email = $('#contact-form #email').val();
     var phone = $('#contact-form #phone').val();
     var mensaje = $('#contact-form #mensaje').val();
     var dataString = 'name='+ name + '&email=' + email + '&phone=' + phone + '&mensaje=' + mensaje;

     $.ajax({
             type: "POST",
             url: $home_url+'process.php',
              data: dataString,
             success: function (data) {
                 $('#contact-form .alert').fadeOut();
                 alert(data);
                 if (data == 'ok') {
                     $('#contact-form .alert-success').fadeIn();
                     $('#contact-form #submit').text('ENVIAR');
                 } else {
                     $('#contact-form .alert-danger.not-send').fadeIn();
                     $('#contact-form #submit').text('ENVIAR');
                 }
             }
         });
 }
 function submitForm_reserva() {
     $('#reserva-form #submit').text('ENVIANDO...');
      var name = $('#reserva-form #name').val();
      var email = $('#reserva-form #email').val();
      var phone = $('#reserva-form #phone').val();
      var paquete = $('#reserva-form #paquete').val();
      var dni = $('#reserva-form #dni').val();
      var birthdate = $('#reserva-form #birthdate').val();
      var mayores = $('#reserva-form #mayores').val();
      var menores = $('#reserva-form #menores').val();
      var edad_menores = $('#reserva-form #edad_menores').val();
      var fecha_salida = $('#reserva-form #fecha_salida').val();
      var mensaje = $('#reserva-form #mensaje').val();

      var dataString = 'name='+ name + '&email=' + email + '&phone=' + phone + '&mensaje=' + mensaje + '&paquete=' + paquete + '&dni=' + dni + '&birthdate=' + birthdate + '&mayores=' + mayores + '&menores=' + menores + '&edad_menores=' + edad_menores + '&fecha_salida=' + fecha_salida;
    $.ajax({
              type: "POST",
              url: $home_url+'process-reserva.php',
              data: dataString,
              success: function (data) {
                  $('#reserva-form .alert').fadeOut();
                  if (data == 'ok') {
                      $('#reserva-form .alert-success').fadeIn();
                      $('#reserva-form #submit').text('ENVIAR');
                  } else {
                      $('#reserva-form .alert-danger.not-send').fadeIn();
                      $('#reserva-form #submit').text('ENVIAR');
                  }
              }
          });
  }

  function submitForm_individual() {
      $('#individual-form #submit').text('ENVIANDO...');
       var name = $('#individual-form #name').val();
       var email = $('#individual-form #email').val();
       var phone = $('#individual-form #phone').val();
       var paquete = $('#individual-form #paquete').val();
       var birthdate = $('#individual-form #birthdate').val();
       var fecha_salida = $('#individual-form #fecha_salida').val();
       var mensaje = $('#individual-form #mensaje').val();
       var dataString = 'name='+ name + '&email=' + email + '&phone=' + phone + '&mensaje=' + mensaje + '&paquete=' + paquete + '&birthdate=' + birthdate + '&fecha_salida=' + fecha_salida;
       $.ajax({
               type: "POST",
               url: $home_url+'process-individual.php',
               data: dataString,
               success: function (data) {
                   $('#individual-form .alert').fadeOut();
                   if (data == 'ok') {
                       $('#individual-form .alert-success').fadeIn();
                       $('#individual-form #submit').text('ENVIAR');
                   } else {
                       $('#individual-form .alert-danger.not-send').fadeIn();
                       $('#individual-form #submit').text('ENVIAR');
                   }
               }
           });
   }

  $('#contact-form').validator().on('submit', function (e) {
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            $('#contact-form .alert-danger.capcha').fadeIn();
        } else {
          if(e.isDefaultPrevented()){
            $('#contact-form .alert-danger.not-send').fadeIn();
          }else{
             e.preventDefault();
             submitForm_contact();
          }
      }
  });
  $('#reserva-form').validator().on('submit', function (e) {
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            $('#reserva-form .alert-danger.capcha').fadeIn();
        } else {
          if(e.isDefaultPrevented()){
            $('#reserva-form .alert-danger.not-send').fadeIn();
          }else{
             e.preventDefault();
             submitForm_reserva();
          }
      }
  });

  $('#individual-form').validator().on('submit', function (e) {
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            $('#individual-form .alert-danger.capcha').fadeIn();
        } else {
          if(e.isDefaultPrevented()){
            $('#individual-form .alert-danger.not-send').fadeIn();
          }else{
             e.preventDefault();
             submitForm_individual();
          }
      }
  });
