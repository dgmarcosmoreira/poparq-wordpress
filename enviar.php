<?php include ('header.php'); ?>
<section class="main">
    <div class="spacer-2"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="line-block">
                    <h2 class="lined">¡Genial! <br> Ya casi terminamos.</h2>
                    <p class="lead">
                        Ingresá tus datos personales para que podamos comunicarnos con vos
                    </p>
                </div>
            </div>
        </div>
        <div class="spacer-1"></div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="email">Localidad</label>
                        <input type="text" class="form-control" id="localidad" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="comentarios">Comentarios</label>
                        <textarea class="form-control" id="comentarios" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-warning btn-block mt-5">ENVIAR</button>
                </form>
            </div>
        </div>
        <div class="spacer-1"></div>
        <div class="row justify-content-center">
            <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-body">
                        <p><strong>ELEGISTE</strong></p>
                        <h2 class="py-0 my-0"><small>Modelo Sauce</small></h2>
                        <p>2 ambientes</p>
                        <h5 class="text-primary font-weight-bold">$468.000</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <p><strong>ADICIONALES</strong></p>
                        <div class="cbox-disabled">
                            <i class="fas fa-check icon"></i>
                            <label for="check-4">Checkbox unchecked</label>
                            <span></span>
                        </div>
                        <div class="cbox-disabled">
                            <i class="fas fa-check"></i>
                            <label for="check-5">Checkbox unchecked</label>
                            <span></span>
                        </div>
                        <div class="cbox-disabled">
                            <i class="fas fa-check"></i>
                            <label for="check-6">Checkbox unchecked</label>
                            <span></span>
                        </div>
                        <div class="cbox-disabled">
                            <i class="fas fa-check"></i>
                            <label for="check-7">Checkbox unchecked</label>
                            <span></span>
                        </div>
                        <div class="cbox-disabled">
                            <i class="fas fa-check"></i>
                            <label for="check-8">Checkbox unchecked</label>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer-2"></div>
</section>
<?php include ('footer.php'); ?>
